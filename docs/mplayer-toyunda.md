# MPlayer-Toyunda

MPlayer Toyunda est une version modifi�e du lecteur de vid�os MPlayer pour lire les karaok�s au format Toyunda (format Epitanime.)

Ce player n'est plus mis � jour depuis des lustres et fonctionne sur une tr�s vieille version de MPlayer qui ne prend pas en compte des choses comme le multi-�crans ou l'acc�l�ration mat�rielle, ou encore le mkv ou le mp4. 

A terme il va dispara�tre de la distribution Toyunda Mugen, lorsque tous les times seront remplac�s par des versions en ASS.

## Sous Windows

Mplayer-toyunda est inclus dans Toyunda Mugen

## Sous macOS

Vous avez perdu, Mplayer-toyunda ne tourne pas sous OSX.

## Sous Linux

Il a besoin d'�tre compil� � la main avant utilisation. Vous trouverez les sources dans app/players/linux/

### Compiler mplayer-toyunda

* D�compressez les sources dans un dossier temporaire
* Sous une distribution � base de Debian/Ubuntu/Mint, installez les packages n�cessaires pour compiler mplayer-toyunda via cette commande (vous devez avoir activez les sources de d�p�t de code source type "deb-src"):
```
apt-get build-dep mpv
apt-get install gcc-4.9 
```
	* Ou toute autre version de GCC 4.x que vous pouvez installer sur votre distribution.
* Dans le dossier des sources, la proc�dure habituelle pour compiler :
```
./configure --cc=/usr/bin/gcc-4.x (indiquez le binaire de votre GCC 4.x)
make
sudo make install
```
* Editer le config.ini de Toyunda Mugen pour indiquer le chemin de Mplayer Toyunda (par d�faut /usr/bin/local)
