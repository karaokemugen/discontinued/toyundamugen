<?php
require_once("config.php");
require_once("helpers.php");

if (!file_exists(temp)) {
    mkdir(temp);
}

//Ecriture du numéro de PID dans un fichier webapp.pid
$WebappPID = getmypid();
unlink('temp/webapp.pid');
file_put_contents('temp/webapp.pid',$WebappPID);

$action=(isset($_GET['action']))?$_GET['action']:'default';

function utf8encode($txt) {
	return mb_convert_encoding($txt, 'UTF-8', mb_detect_encoding($txt. 'a' , 'UTF-8, ISO-8859-1, ASCII'));
}

//Détection de l'OS.
function getOS() {
	$OS = explode(" ",php_uname(S));
	switch ($OS[0]) {
		case "Linux":
			return "Linux";
		case "Windows":
			return "Windows";
		case "Darwin":
			return "OSX";
	}
}

if(empty($_COOKIE['pass'])) $_COOKIE['pass']='';
if ( $action=='disp_admin') {
	if ( $_SERVER['PHP_AUTH_PW']!=$config['Admin']['Password'] ) {
		header('WWW-Authenticate: Basic realm="Authentification (laissez le champ user vide)"');
		header('HTTP/1.0 401 Unauthorized');
		die ('Not authorized <a href="?">Retour</a>');
	} else {
		setcookie('pass',sha1($config['Admin']['Password']), time()+3600*24*365);
		$_COOKIE['pass']=sha1($config['Admin']['Password']);
	}
}
$admin=($_COOKIE['pass']==sha1($config['Admin']['Password']));

// on liste tous les fichiers kara
$list_kara=array();
$list_files = scandir($config['Path']['Kara']);
foreach ($list_files as $key => $file) {
	if(pathinfo ($file,PATHINFO_EXTENSION)=='kara') {
		//On regarde si le kara est dans la blacklist et si la blacklist est activée
		//Si oui aux deux questions, on ajoute pas le kara dans la liste, si non on l'ajoute

		if ( $config['Playlist']['Blacklist'] == 1 ) {
			$Blacklist = trim(file_get_contents($config['Playlist']['BlacklistFile']));
			$Blacklist = $Blacklist?explode("\n",$Blacklist):array();
			$pos = array_search($file,$Blacklist);
			if ( $pos !== false ) {
				// On n'ajoute rien
			} else {
				$list_kara[]=utf8encode(trim(pathinfo($file,PATHINFO_FILENAME)));
			}
		} else {
			$list_kara[]=utf8encode(trim(pathinfo($file,PATHINFO_FILENAME)));
		}
	}
}
$listhtml=json_encode($list_kara,JSON_HEX_APOS);

// Liste des éléments ajoutés dans la playlist
if ($action == 'playlist') {
	$playlist=file_get_contents($config['Playlist']['File']);


	if($admin) {

		$encours=true;
		$playlist=$playlist?explode("\n",$playlist):array();
		foreach($playlist as $key => $item){
			if($item) {
				$item=pathinfo($item,PATHINFO_FILENAME);


				$pos=array_search(trim($item),$list_kara);
				if($encours) {
					//$playlist[$key]= '<div class="listele"><input type="button" name="retirer" value="En cours..." class="disbutton" disabled> '.$item.'</div>';
					$playlist[$key]= '<div class="listele current"><input type="button" name="skip" value="Passer" onclick="skip()" class="button skip"> '.$item.'</div>';
					$encours=false;
				} else {
					$playlist[$key]= '<div class="listele"><input type="button" name="retirer" value="Retirer" onclick="retirer('.$pos.')" class="button rem"> '.$item.'</div>';
				}
			}
		}

		//error_log(print_r($list_kara,true));

		$playlist=implode("\n",$playlist);
		$playlist=nl2br($playlist);

	} else {

		$playlist=nl2br($playlist);

	}

	echo $playlist;
	die();
}

// shuffle de la playlist en cours
if ($action == 'shuffle') {

	// Recuperation de la playlist
	$data=trim(file_get_contents($config['Playlist']['File']));
	$data=explode("\n",$data);

	// Si la playlist est vide, on annule. Sinon on enregistre le premier element
	if(count($data)<1 || $data[0]=="") {
		die('0');
	}
	$firstItem= $data[0];
	unset($data[0]);

	// On mélange le tout et on rajoute le premier element
	shuffle($data);
	array_unshift($data, $firstItem);

	// On ajoute à la playlist (en écrasant l'existante)
	error_log('Shuffle de la playlist existante');
	error_log('Dans: '.$config['Playlist']['File']);
	$data=implode("\n",$data);
	file_put_contents($config['Playlist']['File'],$data."\n");
	file_put_contents($config['Playlist']['File'].'2',$data."\n");
	die('1');
}

// demande d'ajout d'un élément
if ($action == 'additem') {
    $pseudo = $_COOKIE['pseudo'];
    $idfile = (int)$_GET['idfile'];
    $next = (int)$_GET['next'];

    $item_to_add = $list_kara[$idfile] . ".kara";

    error_log(print_r($idfile, true));
    error_log(print_r($item_to_add, true));

    // on vérifie que le fichier n'est pas déjà présent dans la playlist
    $playlist = trim(file_get_contents($config['Playlist']['File']));
    $playlist = $playlist ? explode("\n", $playlist) : array();

	$pos = search_kara_in_playlist($playlist,$item_to_add);

    if ($pos !== false) {
    	ob_start();
		var_dump($pos);
        error_log(ob_get_clean());
        error_log(print_r($playlist, true));
        error_log('Deja present: ' . $item_to_add);
        die('1');
    }

    // Système antiflood
    switch ($config['Playlist']['AntiFloodType']) {
        case "IP":
            $requester = $_SERVER['REMOTE_ADDR'];
            break;
        case "Nickname":
            $requester = $pseudo;
            break;
    }

    // Compter le nombre d'occurences de $requester dans la playlist
    $SongsFromRequester = 0;
    $Recherche = "|" . $requester;
    for ($ligne = 0; $ligne < count($playlist); $ligne++) {
        if (strpos($playlist[$ligne], $Recherche) !== false) {
            $SongsFromRequester++;
        }
    }

    if ($SongsFromRequester >= $config['Playlist']['SongsPerRequester'] && $config['Playlist']['SongsPerRequester'] != "") {
        error_log('Trop de chansons demandées par ' . $requester);
        die('2');
    } else {
        // on l'ajoute
        error_log('Ajout de: ' . $item_to_add);
        error_log('Dans: ' . $config['Playlist']['File']);
        if ($next == 1) {
            error_log('Prochaine chanson.');
            array_splice($playlist, 1, 0, $item_to_add . "|" . $requester);
        } else {
            error_log('Ajoute a la fin de la liste.');
            $playlist[] = $item_to_add . "|" . $requester;
        }
        $playlist = implode("\n", $playlist);
        file_put_contents($config['Playlist']['File'], $playlist . "\n");
        die('0');
    }
}
// demande d'ajout de l'ensemble de la liste
if ($action == 'additemsall') {

	// Ajout des extensions .kara aux titres
	$items_to_add= array();
	foreach ($list_kara as $file) {
		$items_to_add[] = $file.".kara";
	}
	// On mélange le tout
	shuffle($items_to_add);

	// On ajoute à la playlist (en écrasant l'existante)
	error_log('Ajout de toute la liste a la playlist');
	error_log('Dans: '.$config['Playlist']['File']);
	$data=implode("\n",$items_to_add);
	file_put_contents($config['Playlist']['File'],$data."\n");
	die('1');
}
// suppression d'un élément
if ($action == 'remitem') {
	$idfile=$_GET['idfile'];
	$item_to_remove = $list_kara[$idfile].".kara";

	error_log(print_r($idfile,true));
	error_log(print_r($item_to_remove,true));


	// on vérifie que le fichier n'est pas déjà supprimé
	$playlist=trim(file_get_contents($config['Playlist']['File']));
	$playlist=explode("\n",$playlist);

	$pos = search_kara_in_playlist($playlist,$item_to_remove);
	if($pos===false) {
		error_log('Deja supprimé: '.$item_to_remove);
		die('0');
	}
	// s'il n'y est pas, on l'ajoute
	error_log('Retrait de: '.$item_to_remove);
	error_log('Dans: '.$config['Playlist']['File']);
	unset($playlist[$pos]);
	$playlist=implode("\n",$playlist);
	file_put_contents($config['Playlist']['File'],$playlist."\n");
	die('1');
}

// permet de skipper l'element en cours
if ($action == 'skipcurrent') {
	if (getOS() === 'Windows') {
		$outputMpv = shell_exec('taskkill /f /im "mpv.exe"');
		$outputMplayer = shell_exec('taskkill /f /im "mplayer-toyunda.exe"');
	} else {
		$outputMpv = shell_exec('kill -9 "mpv"');
		$outputMplayer = shell_exec('kill -9 "mplayer-toyunda"');
	}
}

// Mettre Toyunda Mugen en pause
if ($action == 'pause') {
	$output = file_put_contents("temp/pause","Supprimez ce fichier pour retirer la pause.");
	if($output) {
		die('1');
	} else {
		die('0');
	}
}




// Mettre Toyunda Mugen en lecture
if ($action == 'play') {
	$output = unlink("temp/pause");
	if($output) {
		die('1');
	} else {
		die('0');
	}
}

if ($action == 'exitToyundaMugen') {
// Arrêt complet Toyunda Mugen
// On a besoin du PID du lecteur, on présume qu'il est lancé
// On a déjà le PID du Webapp.
	$PlayerPID = file_get_contents('temp/player.pid');
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		$outputPlayer = system("taskkill /f /PID ".$PlayerPID);
		$outputWebapp = system("taskkill /f /PID ".$WebappPID);
	} else {
		$outputPlayer = system("kill -9 $PlayerPID");
		$outputWebapp = system("kill -9 $WebappPID");
	}
}





?><!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="default" />

		<link rel="icon" type="image/png" href="images/favicon2.png" />
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="css/webapp.css" media="all" />

		<title>Toyunda Mugen</title>
<style type="text/css" media="all">
#list {
	background-image: url("<?php echo $config['Appearance']['BackgroundImage']; ?>");
}
</style>

</head>
<body>
<script>
var liste=<?php echo $listhtml; ?>;
var timeoutsearch;

function shuffle() {
	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=shuffle', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;
				disp_playlist();
				if (reponse == "1") {
					document.getElementById('footer').innerHTML="<span>Shuffle de la playlist effectué</span>";
				}
				if (reponse == "0") {
					document.getElementById('footer').innerHTML="<span>Echec du shuffle de la playlist (liste vide ?)</span>";
				}
			}
		}
		objHTTP1.send(null);
	}
}

function retirer(i) {
	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=remitem&idfile='+i+'', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;
				disp_playlist();
			}
		}
		objHTTP1.send(null);
	}
}

function ajouterprochaine(i) {

	nomfichierdecode=liste[i]

	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=additem&idfile='+i+'&next=1', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;

				if (reponse == "1") {
					document.getElementById('footer').innerHTML="<span>Ajouté prochaine chanson : "+unescape(nomfichierdecode)+"</span>";
				}
				if (reponse == "0") {
					document.getElementById('footer').innerHTML="<span>Déjà en playlist : "+unescape(nomfichierdecode)+"</span>";
				}
			}
		}
		objHTTP1.send(null);
	}
}

function ajouter(i) {

	nomfichierdecode=liste[i]

	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=additem&idfile='+i+'', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;

				if (reponse == "0") {
					document.getElementById('footer').innerHTML="<span>Ajouté : "+unescape(nomfichierdecode)+"</span>";
				}
				if (reponse == "1") {
					document.getElementById('footer').innerHTML="<span>Déjà en playlist : "+unescape(nomfichierdecode)+"</span>";
				}
				if (reponse == "2") {
					document.getElementById('footer').innerHTML="<span>Vous ne pouvez plus ajouter de chansons pour le moment. (Limité à <?php echo $config['Playlist']['SongsPerRequester']; ?> par personne)</span>";
				}
			}
		}
		objHTTP1.send(null);
	}
}

function ajouterTout() {
	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=additemsall', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;
				disp_playlist();
				if (reponse == "1") {
					document.getElementById('footer').innerHTML="<span>Ajout de l'ensemble des chansons à la playlist</span>";
				}
				if (reponse == "0") {
					document.getElementById('footer').innerHTML="<span>Erreur de l'ajout des chansons à la playlist</span>";
				}
			}
		}
		objHTTP1.send(null);
	}
}

function exitToyundaMugen() {
	alert("Vous pouvez maintenant fermer cet onglet après avoir cliqué sur OK");
	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=exitToyundaMugen', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;
				if (reponse == "1") {
					document.getElementById('footer').innerHTML="<span>ToyundaMugen est arrêté. Vous pouvez fermer le navigateur.</span>";
				}
				if (reponse == "0") {
					document.getElementById('footer').innerHTML="<span>Impossible de quitter. Bienvenue dans Sword Art Online.</span>";
				}
			}
		}
		objHTTP1.send(null);
	}
}function play() {
	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=play', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;

				if (reponse == "1") {
					document.getElementById('pauseplay-content').innerHTML='<button onclick="pause()"><img src="images/pause.png"><span class="mobile_hide"> Pause</span></button>';
					document.getElementById('footer').innerHTML="<span>Reprise de la lecture de la playlist.</span>";
				}
				if (reponse == "0") {
					document.getElementById('footer').innerHTML="<span>Erreur de fonction play</span>";
				}
			}
		}
		objHTTP1.send(null);
	}
}


function pause() {
	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=pause', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;

				if (reponse == "1") {
					document.getElementById('pauseplay-content').innerHTML='<button onclick="play()"><img src="images/play.png"><span class="mobile_hide"> Play</span></button>';
					document.getElementById('footer').innerHTML="<span>Pause de la lecture de la playlist.</span>";
				}
				if (reponse == "0") {
					document.getElementById('footer').innerHTML="<span>Erreur de fonction pause</span>";
				}
			}
		}
		objHTTP1.send(null);
	}
}
function skip() {
	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=skipcurrent', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;
				disp_playlist();
			}
		}
		objHTTP1.send(null);
	}
}

function disp_addlist() {
	document.getElementById('button_disp_playlist').style.display='initial';
	document.getElementById('button_disp_addlist').style.display='none';

	document.getElementById('liste_id').style.display='initial';
	document.getElementById('playliste_id').style.display='none';

	document.getElementById('footer').style.display='initial';
	document.getElementById('footer2').style.display='none';

}

function disp_playlist() {
	var objHTTP1 = new XMLHttpRequest();
	if (typeof objHTTP1 == 'object') {
		objHTTP1.open('GET', '?action=playlist', true);
		objHTTP1.onreadystatechange = function() {
			if (objHTTP1.readyState == 4 && objHTTP1.status == 200) {
				var reponse=objHTTP1.responseText;

				document.getElementById('button_disp_playlist').style.display='none';
				document.getElementById('button_disp_addlist').style.display='initial';

				document.getElementById('liste_id').style.display='none';
				document.getElementById('playliste_id').style.display='initial';
				document.getElementById('playliste_id').innerHTML=reponse;



				document.getElementById('footer').style.display='none';
				document.getElementById('footer2').style.display='initial';



			}
		}
		objHTTP1.send(null);
	}
}


function playRandom(nb=1) {
	var min= 0;
	var max= Math.floor(document.querySelectorAll('.listele').length - 1);
	var randChoice= Math.floor(Math.random() * (max - min) + min);

	for (var i = 0; i < nb; i++) {
		randChoice= Math.floor(Math.random() * (max - min) + min);
		ajouter(randChoice);
	}
}


function filter_search_delay() {
    timeoutsearch=setTimeout("filter_search()",800);
}

function filter_search() {
	filter_value=document.getElementById("searchbox_id").value
	listehtml="";
	regExpStr=filter_value.replace(/([\.\/\]\[\{\}\(\)\\\$\^\?\*\|\!\=\+\-])/g, '\\$1');
	var regExp = new RegExp(regExpStr, "ig");


	FR=document.getElementById("lang_fr").checked;
	EN=document.getElementById("lang_ang").checked;
	JP=document.getElementById("lang_jap").checked;
	DIV=document.getElementById("lang_div").checked;



	console.log(filter_value)

	listehtml="";

	for(i in liste) {

		if(liste[i].search(regExp) != -1 || filter_value=="") {
			langfichier=liste[i];
			langfichier=langfichier.split(" ");
			langfichier=langfichier[0];

			add=0;
			if (langfichier == "FR" && FR) { add=1; }
			if (langfichier == "ANG" && EN) { add=1; }
			if (langfichier == "JAP" && JP) { add=1; }
			if (langfichier != "FR" &&
				langfichier != "ANG" &&
				langfichier != "JAP" && DIV) { add=1; }
			if (add == 1) {
				listehtml+='<div class="listele">';
				<?php
					if ($admin) {
						?>
						listehtml+='<input type="button" name="ajouter_prochaine" value="Prochaine" onclick="ajouterprochaine('+i+')" class="button add">';
						<?php
					}
				?>
				listehtml+='<input type="button" name="ajouter" value="Ajouter" onclick="ajouter('+i+')" class="button add"> - '+liste[i]+'</div>';
			}
		}

	}

	document.getElementById('liste_id').innerHTML=listehtml;

}


function update_lng() {
	FR=document.getElementById("lang_fr").checked;
	JP=document.getElementById("lang_jap").checked;
	EN=document.getElementById("lang_ang").checked;
	DIV=document.getElementById("lang_div").checked;
	document.getElementById("tabl_lang_fr").style.color=FR?'#000':'#888';
	document.getElementById("tabl_lang_jap").style.color=JP?'#000':'#888';
	document.getElementById("tabl_lang_ang").style.color=EN?'#000':'#888';
	document.getElementById("tabl_lang_div").style.color=DIV?'#000':'#888';
	filter_search();
}


function lng_disp_table() {
	if(document.getElementById("lng_box").style.display=="block") {
		document.getElementById("lng_box").style.display="none";
	} else {

		FR=document.getElementById("lang_fr").checked;
		JP=document.getElementById("lang_jap").checked;
		EN=document.getElementById("lang_ang").checked;
		DIV=document.getElementById("lang_div").checked;


		document.getElementById("tab_lang_fr").checked=FR;
		document.getElementById("tab_lang_jap").checked=JP;
		document.getElementById("tab_lang_ang").checked=EN;
		document.getElementById("tab_lang_div").checked=DIV;


		document.getElementById("lng_box").style.display="block";
	}
}
function close_lng() {
	document.getElementById("lng_box").style.display="none";
}

function tab_update_lng(ele,num) {
	if(num==1) document.getElementById("lang_fr").checked=ele.checked;
	if(num==2) document.getElementById("lang_jap").checked=ele.checked;
	if(num==3) document.getElementById("lang_ang").checked=ele.checked;
	if(num==4) document.getElementById("lang_div").checked=ele.checked;
	update_lng();

}

function disp_admin() {
	window.location="?action=disp_admin";
}

function setCookie(c_name,value)
{
	var expiry=new Date();
	expiry.setDate(expiry.getDate() + 100);
	document.cookie=c_name + "=" + escape(value);
}

function demander_pseudo() {
	var pseudo = prompt("Entrez votre pseudo","");
	setCookie("pseudo",pseudo);
    location.reload();
}


</script>

<?php

if (empty($_COOKIE['pseudo'])) {
	?>
	<script>demander_pseudo();</script>
	<?php
}
?>

<!-- icones trouvées ici: https://icons8.com/web-app/for/color/shuffle https://icons8.com/web-app/20444/bulleted-list -->

<div id="lng_box" style="display:none" class="lng_box">
		<div style="text-align:right"><input type="button" onclick="close_lng()" value="Fermer"> </div>
		<input type="checkbox" checked id="tab_lang_fr" onchange="tab_update_lng(this,1)" ><label for="tab_lang_fr">FR</label><br>
		<input type="checkbox" checked id="tab_lang_jap" onchange="tab_update_lng(this,2)"><label for="tab_lang_jap">JAP</label><br>
		<input type="checkbox" checked id="tab_lang_ang" onchange="tab_update_lng(this,3)"><label for="tab_lang_ang">ANG</label><br>
		<input type="checkbox" checked id="tab_lang_div" onchange="tab_update_lng(this,4)"><label for="tab_lang_div">DIV</label><br>

</div>


<div id="header">
	<!-- Preload image -->
	<img src="images/add.png" style="position:absolute;left:-9999px">


	<table id="headertable"><tr>

            <?php
            if($config['Playlist']['AntiFloodType'] == "Nickname") {
                ?>
                <td>
                    <button onclick="demander_pseudo()"><i class="fa fa-address-card" aria-hidden="true"></i></button>
                </td>
                    <?php echo empty($_COOKIE['pseudo'])?"" : "<td>".$_COOKIE['pseudo']."</td>"; ?>
                <?php
            }
            ?>
	<td><button id="button_disp_playlist" onclick="disp_playlist()"><img src="images/list_playlist.png"><span class="mobile_hide"> Afficher la Playlist</span></button></td>
	<td><button id="button_disp_addlist" style="display:none;" onclick="disp_addlist()"><img src="images/list_add.png"><span class="mobile_hide"> Afficher les titres à ajouter</span></button></td>


	<td><input type="text" id="searchbox_id" onkeyup="filter_search_delay()" onchange="filter_search_delay()" style="height:30px;width:110px;" class="searchlield">
	<!-- <button onclick="displist()"><img src="images/Search-48.png"></button> --></td>




	<td><span class="mobile_only">
		<table id="table_lng" onclick="lng_disp_table();" style="cursor:pointer;">
			<tr>
				<td id="tabl_lang_fr">FR</td>
				<td id="tabl_lang_jap">JAP</td>
			</tr>
			<tr>
				<td id="tabl_lang_ang">ANG</td>
				<td id="tabl_lang_div">DIV</td>
			</tr>
		</table>
	</span></td>

	<td><span class="mobile_hide">
		<input type="checkbox" checked id="lang_fr" onchange="update_lng()"><label for="lang_fr">FR</label>
		<input type="checkbox" checked id="lang_jap" onchange="update_lng()"><label for="lang_jap">JAP</label>
		<input type="checkbox" checked id="lang_ang" onchange="update_lng()"><label for="lang_ang">ANG</label>
		<input type="checkbox" checked id="lang_div" onchange="update_lng()"><label for="lang_div">DIV</label>
	</span>
	<?php if($admin) { ?>
	<td><button onclick="playRandom()"><img src="images/chance.png"><span class="mobile_hide"> J'ai de la chance !</span></button></td>
	<td><button onclick="ajouterTout()"><img src="images/add_all.png"><span class="mobile_hide"> Tout ajouter</span></button></td>
	<td><button onclick="shuffle()"><img src="images/Shuffle-48.png"><span class="mobile_hide"> Shuffle</span></button></td>
		<?php if(file_exists("temp/pause")) { ?>
			<td id="pauseplay-content"><button onclick="play()"><img src="images/play.png"><span class="mobile_hide"> Play</span></button></td>
		<?php } else { ?>
			<td id="pauseplay-content"><button onclick="pause()"><img src="images/pause.png"><span class="mobile_hide"> Pause</span></button></td>
		<?php } ?>
	<td><button onclick="exitToyundaMugen()"><img src="images/standby-power-icone-9785-48.png"><span class="mobile_hide"> Arrêter Toyunda Mugen</span></button></td>
	<?php } ?>
	</tr></table>

</div>



<div id="list">
	<div id="liste_id"> Chargement... </div>
	<div id="playliste_id" style="display:none"> Chargement... </div>
</div>



<div id="footer"></div>
<div id="footer2" style="display:none">

	<table><tr>



	<td style="white-space: nowrap"><button onclick="disp_playlist()"><img src="images/Refresh.png"><span class="mobile_hide"> Actualiser</span></button></td>
	<td style="width:100%;"></td>
	<td style="white-space: nowrap"><button onclick="disp_admin()"><img src="images/cle-icone-4803-32.png"><span class="mobile_hide"> Admin</span></button></td>

	</tr></table>



</div>

<script>
setTimeout(function(){ update_lng(); }, 100);
</script>

</body>
</html>