<?php

$dir='./Lyrics';


error_reporting(E_ALL);
ini_set('display_errors', '1');


function to_unix($data) {
	return str_replace("\r","\n",str_replace("\r\n","\n",$data));
}




$files=scandir($dir);


foreach($files as $file) {
	$filepathinfo=pathinfo ($dir.'/'.$file);
	$ext=$filepathinfo['extension'];
	$base=$filepathinfo['filename'];
	
	if($ext=='vtt') {
		echo 'traitement de: '.$file."\n";
		
		
		
		$current_file=$dir.'/'.$file;
		$contenu=file_get_contents($current_file);
		
		$contenu=to_unix($contenu);
		
		$contenu=explode("\n",$contenu);
		foreach($contenu as $numline => $valline) {
			if(trim($valline)=='WEBVTT') {
				$contenu[$numline]='';
			}
		}
		$contenu=implode("\n",$contenu);
		$contenu=trim($contenu);
		
		
		
		
		$contenu=explode("\n",$contenu);
		$i=0;
		foreach($contenu as $numline => $valline) {
			if(!isset($contenu[$numline-1]) || trim($contenu[$numline-1])=='') {
				$contenu[$numline]=++$i."\n".$contenu[$numline];
			}
			
			
			if(strpos($valline,' --> ')!==false) {
				
				$contenu[$numline]=str_replace('.',',',$contenu[$numline]);
				$data=explode(' ',$contenu[$numline]);
				$contenu[$numline]=$data[0].' --> '.$data[2];
				
			}
		}
		$contenu=implode("\n",$contenu);
		
		
		
		
		
		$contenu2=explode("\n",$contenu);
		$deb=0;
		$time_deb='';
		$time_fin='';
		$cont=array();
		$out=array();
		$i=1;
		
		foreach($contenu2 as $numline => $valline) {
			
			if(strpos($valline,' --> ')!==false) {
				$deb=1;
				$time=explode(' --> ',$valline);
				$time_deb=trim(str_replace(',','.',$time[0]));
				$time_fin=trim(str_replace(',','.',$time[1]));
				$time_deb=substr($time_deb,0,-1);
				$time_fin=substr($time_fin,0,-1);
				continue;
			}
			if($deb==1) {
				if(trim($valline)!='') {
					$cont[]=trim($valline);
				} else {
					$out[]='Dialogue: 1,'.$time_deb.','.$time_fin.',DefaultVCD, NTP,0000,0000,0000,,{\an8}'.implode('\N',$cont);
					
					++$i;
					$deb=0;
					$time_deb='';
					$time_fin='';
					$cont=array();
				}
				
			}
		}
		
		
		
		
		
		$contenu2='[Script Info]
Title: '.$base.'
ScriptType: v4.00+
Collisions: Reverse
 
[V4+ Styles]
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding
Style: DefaultVCD,Arial,20,&H00FFFFFF,&H000088EF,&H00000000,&H00666666,-1,0,0,0,100,100,0,0,1,3,0,8,10,10,10,1
 
[Events]
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text
';
		
		$contenu2.=implode("\n",$out);
		
		
		
		$new_file=$dir.'/'.$base.'.srt';
		file_put_contents($new_file,$contenu);
		
		
		$new_file=$dir.'/'.$base.'.ass';
		file_put_contents($new_file,$contenu2);
		
		
		
	}
}
?>