/************************\
*        Epitass         *
* parser_toyunda_raw.mly *
*                        *
*  parsing toyunda  raw  *
\************************/


  /* Header (Ocaml declarations) */
%{
  open Tree_toyunda_raw;;  
%}



  /* Ocamlyacc declarations */
  %token RETURN
  %token OPEN_BRACE
  %token <string> NUM
  %token CLOSE_BRACE
  %token SPACE
  %token COLOR_SUBTITLE_OPTION
  %token <string> COLOR_FIELD
  %token COLON
  %token SIZE_SUBTITLE_OPTION
  %token POSITION_SUBTITLE_OPTION
  %token COMA
  %token BITMAP_SUBTITLE_OPTION
  %token DDR_SUBTITLE_OPTION
  %token <string> DDR_INSTRUCTION
  %token PIPE
  %token COLOR_OPTION
  %token SIZE_OPTION
  %token POSITION_OPTION
  %token BITMAP_OPTION
  %token DDR_OPTION
  %token <string> NSCS
  %token EOF

  %start kara
  %type <Tree_toyunda_raw.t> kara



%%



  /* Grammar rules */


  kara :
    |   comment EOF
          { [] }
    ;
    |   subtitle EOF
          { [$1] }
    ;
    |   comment RETURN kara
          { $3 }
    ;
    |   subtitle RETURN kara
          { $1::$3 }
    ;

  subtitle :
        begin_frame spaces OPEN_BRACE NUM CLOSE_BRACE subtitle_options lines
          { ($1, (int_of_string $4), $6, $7) }
    ;

  begin_frame :
    |   OPEN_BRACE CLOSE_BRACE
          { Begin_follow_previous }
    |   OPEN_BRACE NUM CLOSE_BRACE
          { Begin_frame (int_of_string $2) }
    ;

  spaces :
    |
          {()}
    |   SPACE spaces
          {()}
    ;

  /*shift/reduce conflict here, should not be a problem*/
  subtitle_options :
    |
          { [] }
    |   subtitle_option CLOSE_BRACE subtitle_options
          { $1::$3 }
    ;

  subtitle_option :
    |   COLOR_SUBTITLE_OPTION COLOR_FIELD
          { Subtitle_color_fixed (parse_color $2) }
    |   COLOR_SUBTITLE_OPTION COLOR_FIELD COLON COLOR_FIELD
          { Subtitle_color_progressive (parse_color $2,parse_color $4) }
    |   SIZE_SUBTITLE_OPTION NUM
          { Subtitle_size_fixed (int_of_string $2) }
    |   SIZE_SUBTITLE_OPTION NUM COLON NUM
          { Subtitle_size_progressive (int_of_string $2,int_of_string $4) }
    |   POSITION_SUBTITLE_OPTION position
          { Subtitle_position_fixed $2 }
    |   POSITION_SUBTITLE_OPTION position COLON position
          { Subtitle_position_progressive ($2,$4) }
    |   BITMAP_SUBTITLE_OPTION file_name
          { Subtitle_bitmap($2) }
    |   DDR_SUBTITLE_OPTION DDR_INSTRUCTION
          { Subtitle_ddr $2 }
    ;

  position :
        NUM COMA NUM
          { (int_of_string $1,int_of_string $3) }
    ;

  lines :
    |   pipes line_options text
          { [($1, $2, $3)] }
    |   pipes line_options text PIPE other_lines
          { ($1, $2, $3)::$5 }
    ;

  other_lines :
    |   pipes line_options text
          { [(1 + $1, $2, $3)] }
    |   pipes line_options text PIPE other_lines
          { (1 + $1, $2, $3)::$5 }
    ;

  /*shift/reduce conflict here, should not be a problem*/
  pipes :
    |
          { 0 }
    |   PIPE pipes
          { 1 + $2 }
    ;

  /*shift/reduce conflict here, should not be a problem*/
  line_options :
    |
          { [] }
    |   line_option CLOSE_BRACE line_options
          { $1::$3 }
    ;

  line_option :
    |   COLOR_OPTION COLOR_FIELD
          { Line_color_fixed (parse_color $2) }
    |   COLOR_OPTION COLOR_FIELD COLON COLOR_FIELD
          { Line_color_progressive (parse_color $2, parse_color $4) }
    |   SIZE_OPTION NUM
          { Line_size_fixed (int_of_string $2) }
    |   SIZE_OPTION NUM COLON NUM
          { Line_size_progressive (int_of_string $2,int_of_string $4) }
    |   POSITION_OPTION position
          { Line_position_fixed $2 }
    |   POSITION_OPTION position COLON position
          { Line_position_progressive ($2,$4) }
    |   BITMAP_OPTION file_name
          { Line_bitmap $2 }
    |   DDR_OPTION DDR_INSTRUCTION
          { Line_ddr $2 }
    ;


  /* Garbage : I don't know how to make the lexer work with the really permissive grammar, so I "fixed" it in the lexer. If you now a clean way to do it, let me now */

  comment :
    | commentb {()}
    ;

  commentb:
    | {()}
    | NUM comr {()}
    | CLOSE_BRACE comr {()}
    | SPACE comr {()}
    | COLOR_SUBTITLE_OPTION comr {()}
    | COLOR_FIELD comr {()}
    | COLON comr {()}
    | SIZE_SUBTITLE_OPTION comr {()}
    | POSITION_SUBTITLE_OPTION comr {()}
    | COMA comr {()}
    | BITMAP_SUBTITLE_OPTION comr {()}
    | DDR_SUBTITLE_OPTION comr {()}
    | DDR_INSTRUCTION comr {()}
    | PIPE comr {()}
    | COLOR_OPTION comr {()}
    | SIZE_OPTION comr {()}
    | POSITION_OPTION comr {()}
    | BITMAP_OPTION comr {()}
    | DDR_OPTION comr {()}
    | NSCS comr {()}
    ;

  comr :
    | {()}
    | OPEN_BRACE comr {()}
    | NUM comr {()}
    | CLOSE_BRACE comr {()}
    | SPACE comr {()}
    | COLOR_SUBTITLE_OPTION comr {()}
    | COLOR_FIELD comr {()}
    | COLON comr {()}
    | SIZE_SUBTITLE_OPTION comr {()}
    | POSITION_SUBTITLE_OPTION comr {()}
    | COMA comr {()}
    | BITMAP_SUBTITLE_OPTION comr {()}
    | DDR_SUBTITLE_OPTION comr {()}
    | DDR_INSTRUCTION comr {()}
    | PIPE comr {()}
    | COLOR_OPTION comr {()}
    | SIZE_OPTION comr {()}
    | POSITION_OPTION comr {()}
    | BITMAP_OPTION comr {()}
    | DDR_OPTION comr {()}
    | NSCS comr {()}
    ;


  file_name :
    | fn {String.concat "" $1}
    ;

  fn :
    | {[""]}
    | OPEN_BRACE fn {"{"::$2}
    | NUM fn {$1::$2}
    | SPACE fn {" "::$2}
    | COLOR_SUBTITLE_OPTION fn {"{C:"::$2}
    | COLOR_FIELD fn {$1::$2}
    | COLON fn {":"::$2}
    | SIZE_SUBTITLE_OPTION fn {"{S:"::$2}
    | POSITION_SUBTITLE_OPTION fn {"{P"::$2}
    | COMA fn {","::$2}
    | BITMAP_SUBTITLE_OPTION fn {"{B:"::$2}
    | DDR_SUBTITLE_OPTION fn {"{D:"::$2}
    | DDR_INSTRUCTION fn {$1::$2}
    | PIPE fn {"|"::$2}
    | COLOR_OPTION fn {"{c:"::$2}
    | SIZE_OPTION fn {"{s:"::$2}
    | POSITION_OPTION fn {"{p:"::$2}
    | BITMAP_OPTION fn {"{b:"::$2}
    | DDR_OPTION fn {"{d:"::$2}
    | NSCS fn {$1::$2}
    ;

  text :
    | txtb { String.concat "" $1 }
    ;

  txtb :
    | {[]}
    | NUM txt {$1::$2}
    | CLOSE_BRACE txt {"}"::$2}
    | SPACE txt {" "::$2}
    | COLOR_SUBTITLE_OPTION txt {"{C:"::$2}
    | COLOR_FIELD txt {$1::$2}
    | COLON txt {":"::$2}
    | SIZE_SUBTITLE_OPTION txt {"{S:"::$2}
    | POSITION_SUBTITLE_OPTION txt {"{P"::$2}
    | COMA txt {","::$2}
    | BITMAP_SUBTITLE_OPTION txt {"{B:"::$2}
    | DDR_SUBTITLE_OPTION txt {"{D:"::$2}
    | DDR_INSTRUCTION txt {$1::$2}
    | COLOR_OPTION txt {"{c:"::$2}
    | SIZE_OPTION txt {"{s:"::$2}
    | POSITION_OPTION txt {"{p:"::$2}
    | BITMAP_OPTION txt {"{b:"::$2}
    | DDR_OPTION txt {"{d:"::$2}
    | NSCS txt {$1::$2}
    ;

  txt :
    | {[""]}
    | OPEN_BRACE txt {"{"::$2}
    | NUM txt {$1::$2}
    | CLOSE_BRACE txt {"}"::$2}
    | SPACE txt {" "::$2}
    | COLOR_SUBTITLE_OPTION txt {"{C:"::$2}
    | COLOR_FIELD txt {$1::$2}
    | COLON txt {":"::$2}
    | SIZE_SUBTITLE_OPTION txt {"{S:"::$2}
    | POSITION_SUBTITLE_OPTION txt {"{P"::$2}
    | COMA txt {","::$2}
    | BITMAP_SUBTITLE_OPTION txt {"{B:"::$2}
    | DDR_SUBTITLE_OPTION txt {"{D:"::$2}
    | DDR_INSTRUCTION txt {$1::$2}
    | COLOR_OPTION txt {"{c:"::$2}
    | SIZE_OPTION txt {"{s:"::$2}
    | POSITION_OPTION txt {"{p:"::$2}
    | BITMAP_OPTION txt {"{b:"::$2}
    | DDR_OPTION txt {"{d:"::$2}
    | NSCS txt {$1::$2}
    ;
