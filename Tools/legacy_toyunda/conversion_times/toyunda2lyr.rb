#!/usr/bin/env ruby
##
## toyunda2lyr.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Sun Apr 25 19:04:22 2004 Olivier Leclant
## $Id: toyunda2lyr.rb 1.2 Fri, 23 Jul 2004 19:07:53 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

# Get the lyrics back from a txt toyunda file.
# Does not support yet mikomi format.
# Does not yet get back the "&".

ToyundaChar = "�"

RegExp = /^\{([0-9]*)\}\{([0-9]*)\}\|*\{.*\}([^#{ToyundaChar}]*)$/

lyrics = []

while line = gets do
  if (m = line.match(RegExp)) then
    lyrics << m[3]
  end
end

puts lyrics.uniq
