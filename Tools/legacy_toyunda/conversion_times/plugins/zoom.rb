##
## zoom.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Aug 27 18:19:53 2004 Olivier Leclant
## $Id: zoom.rb 1.2 Mon, 27 Sep 2004 11:15:36 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

module ToyundaGen
  module Styles
    module SupportedStyles

      # note : ce style est moisi, il faut le refaire avec {o:} pour centrer la lettre apr�s zoom
      class Zoom < Style
        def init(vars)
          vars.declare("zoom_max_size", Types::PositiveInt.new(400))
        end
        def process_syl(writer, line, i)
          j = i
          until line.syls[j].has_frames? or line.syls[j].nil? do j += 1 end
          if not line.syls[j].nil? then
            start, stop = line.syls[j].frames
            str = line.only_syl(i)
            writer.add([line.start - line.vars.fade_before, line.start], line.colors[0].fade_in, str)
            writer.add([line.start, start], line.colors[0], str)
            writer.add([start, stop], line.colors[1], "{s:25:#{line.vars.zoom_max_size}}", str)
            writer.add([stop, line.syls[j].stop + line.vars.fade_after], line.colors[2].fade_out, "{s:#{line.vars.zoom_max_size}}", str)
          end
        end
      end
    end
  end
end
