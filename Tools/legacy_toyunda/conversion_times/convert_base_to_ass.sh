# Analyser .ini

CHEMIN="../../../app/data"
INIFILES=`ls $CHEMIN/ini/*.ini`
mkdir -p fps ini kara ass ass_warnings
rm erreurs.txt
rm erreurs_nofps.txt
rm erreurs_notv3.txt
rm converted.txt
rm erreurs_warnings.txt
while read INIFILE
do
	grep .txt "$INIFILE" 2>/dev/null >/dev/null
	if [ $? -eq 0 ]
	then
		echo "Conversion $INIFILE"
		LYRICS=`grep -ai '^file' "$INIFILE" | awk -F= {'print $2'} | tr -d '\n' | tr -d '\r' | sed -e 's/^[[:space:]]*//'`
		KARA=`basename "$LYRICS" .txt`
		INIFILEBASE=`basename "$INIFILE"`
		VIDEO=`grep -ai aviname "$INIFILE" | awk -F= {'print $2'} | tr -d '\n' | tr -d '\r' | sed -e 's/^[[:space:]]*//'`
		#Detection si c'est un timing v3		
		grep "LYRICS - GENERATE AGAIN AFTER YOU EDIT" "$CHEMIN/lyrics/$LYRICS" 2>/dev/null >/dev/null
		if [ $? -eq 0 ]
		then
			#C'est un timing v3, il va falloir extraire. D'abord le .lyr
			# Récupérer lignes entre :
			# # --- LYRICS - GENERATE AGAIN AFTER YOU EDIT ---
			# et
			# # --- SUB - DO NOT EDIT HERE - MODIFICATIONS WILL BE LOST ---
			# virer les -- en début de ligne
			# enregistrer en .lyr

			sed -n '/# --- LYRICS - GENERATE AGAIN AFTER YOU EDIT ---/,/# --- SUB - DO NOT EDIT HERE - MODIFICATIONS WILL BE LOST ---/p' "$CHEMIN/lyrics/$LYRICS" |  sed 's/^--//g' >"kara/$KARA.lyr"
			#Ensuite le .frm
			# # --- TIMING - GENERATE AGAIN AFTER YOU EDIT ---
			# et 
			# fin du fichier
			# supprimer les ==
			# enregistrer en .frm

			sed -n '/# --- TIMING - GENERATE AGAIN AFTER YOU EDIT ---/,$p' "$CHEMIN/lyrics/$LYRICS" | sed 's/^==//g' >"kara/$KARA.frm"

			# On va faire un coup d'iconv 
			rm temp.frm
			rm temp.lyr
			iconv -f iso-8859-1 -t utf-8 "kara/$KARA.frm" >temp.frm
			iconv -f iso-8859-1 -t utf-8 "kara/$KARA.lyr" >temp.lyr
			cp -f temp.frm "kara/$KARA.frm"
			cp -f temp.lyr "kara/$KARA.lyr"

			# On récupère le framerate et la résolution
			ffmpeg -i "$CHEMIN/videos/$VIDEO" 2>fps.txt
			FPS=`cat fps.txt | grep -a fps | awk -F, '{for(i=1;i<=NF;i++){if ($i ~ /fps/){print $i}}}' | awk -F\  {'print $1'}`
			if [ "$FPS" == "" ]
			then
				echo "$INIFILEBASE" >>erreurs_nofps.txt
				cp fps.txt "fps/$INIFILEBASE.txt"
				FPS=""
			else
				rm fps.txt
				echo "ruby toyundagen2ass.rb \"kara/$KARA.lyr\" \"kara/$KARA.frm\" $FPS"
				ruby toyundagen2ass.rb -k "kara/$KARA.lyr" "kara/$KARA.frm" $FPS >"ass/$KARA.ass"			
				RC=$?
				if [ $RC -ne 0 ]
				then
					echo "$INIFILEBASE|Toyundagen2ass" >>erreurs.txt
				else
					#Le Kara a bien été converti, maintenant il faut remplacer les infos
					#Editer le .ini pour remplacer le txt en ass
					#sed -i 's/.txt/.ass/g' "$INIFILE"
					#copier le ass dans lyrics
					cp -f "ass/$KARA.ass" $CHEMIN/lyrics/
					echo "$INIFILEBASE|Toyundagen2ass" >>converted.txt
				fi
			fi
			echo "====================================="
		else 
			# On va faire un coup d'iconv 
			rm temp.txt
			iconv -f iso-8859-1 -t utf-8 "$CHEMIN/lyrics/$LYRICS" >temp.txt
			cp "$CHEMIN/lyrics/$LYRICS" "kara/$KARA.txt"
			cp -f temp.txt "kara/$KARA.txt"

			#On détermine si c'est un v1 ou v2.
			grep -a "{c:" "$CHEMIN/lyrics/$LYRICS" >/dev/null
			if [ $? -eq 0 ]
			then
				OPTIONS="--v2"
			else
				OPTIONS="--v1"
			fi
			ffmpeg -i "$CHEMIN/videos/$VIDEO" 2>fps.txt
			FPS=`cat fps.txt | grep -a fps | awk -F, '{for(i=1;i<=NF;i++){if ($i ~ /fps/){print $i}}}' | awk -F\  {'print $1'}`
			RESX=`cat fps.txt | grep -a fps | awk -F, {'print $3'} | awk -Fx {'print $1'} | sed -e 's/^[[:space:]]*//'`
			RESY=`cat fps.txt | grep -a fps | awk -F, {'print $3'} | awk -Fx {'print $2'} | awk -F\  {'print $1'}`
			if [ "$FPS" == "" ]
			then
				cp fps.txt "fps/$INIFILEBASE.txt"
				echo "$INIFILEBASE ($FPS)" >>erreurs_nofps.txt
			else
				rm fps.txt	
				#Si c'est un timing v1 alors il faut utiliser --v1 ou --v2	
				echo "EpitASS/epitass.standalone $OPTIONS -playresx $RESX -playresy $RESY -video $CHEMIN/videos/$VIDEO -framerate $FPS -o ass/$KARA.ass kara/$KARA.txt"
				EpitASS/epitass.standalone $OPTIONS -playresx $RESX -playresy $RESY -video "$CHEMIN/videos/$VIDEO" -framerate $FPS -o "ass/$KARA.ass" "kara/$KARA.txt" 2>stderr.txt
				RC=$?
				grep Warning stderr.txt
				if [ $? -eq 0 ]
				then
					echo "$INIFILEBASE" >>erreurs_warnings.txt
					RC=2
					rm stderr.txt
				fi
				if [ $RC -ne 0 ]
				then
					echo "$INIFILEBASE|EpitASS" >>erreurs.txt
				else
					echo "Succès!"
					#Le Kara a bien été converti, maintenant il faut remplacer les infos
					#Editer le .ini pour remplacer le txt en ass
					cp -f "$INIFILE" "ini/$INIFILEBASE"					
					sed -i 's/.txt/.ass/g' "ini/$INIFILEBASE"
					#copier le ass dans lyrics
					#cp -f "ass/$KARA.ass" $CHEMIN/lyrics/
					echo "$INIFILEBASE|EpitASS" >>converted.txt
				fi
			fi
		echo "====================================="
		fi
	fi
done <<< "$INIFILES"

