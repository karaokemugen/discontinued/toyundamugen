#!/usr/bin/perl
# This software is under the same licence as perl
# Sylvain "Skarsnik" Colinet
use strict;
use File::Spec::Functions;
use File::Temp qw/tempfile/;
use File::Basename;
# need to desactive this when finish, it just prevent typo error
# handle all warning, create too much redondance
use warnings;

# Config
my $TRADS_DIR = 'Trads';
my $LYRICS_DIR = 'Lyrics';
my $VIDEOS_DIR = 'Videos';
my $LOGOS_DIR = 'Logos';
my $TOYUNDA_PLAYER = 'mplayer-toyunda';
my $KARAOKE_DIR = $ENV{KARAOKE_DIR};
my $TRAD = 0;

#Help
sub print_help {
  print <<EOH;

  usage : 
	 $0 [options] file.ini
	 $0 [options] file.pls
	 $0 [options] file1.ini file2.pls file3.ini, ....

  Options :
          --karaoke-path=path to specifie the karaoke dir
          --help display this help message
	  --infos display info about this script config
	  --trad use a traduction file if exists in KARAOKE_PATH/$TRADS_DIR
EOH
  exit 1;
}

#own die function, can be useful on win32
sub toy_die {
  print $^O, "\n";
  if ($^O =~ /win32/i) {
    print shift, "\n";
    my $a = <STDIN>;
  }
  die shift;
}

sub infos {
  print @_, "\n";
}
#arguments checking
# I can use Getopt::Long, but I prefer have
# minimal dependencie when compile the win32 binarie
# I may will use it later
print_help if grep /^--help$/, @ARGV or !@ARGV;
if (grep /^--infos$/, @ARGV) {
  print <<EOI;

  Configuration :
  name of the script : $0
  \$TRADS_DIR        : $TRADS_DIR
  \$LYRICS_DIR       : $LYRICS_DIR
  \$VIDEOS_DIR       : $VIDEOS_DIR
  \$LOGOS_DIR        : $LOGOS_DIR
  \$TOYUNDA_PLAYER   : $TOYUNDA_PLAYER
  \$KARAOKE_DIR      : $KARAOKE_DIR
  
EOI
  exit 1;
}
# Yes I like grep ^^
if (my @tab = grep /^--karaoke-path=.*$/, @ARGV) {
  ($KARAOKE_DIR) = $tab[0] =~ /^--karaoke-path=(.*)$/;
  @ARGV = grep { $_ !~ /^--karaoke-path=/ } @ARGV;
}
if (grep /^--trad$/, @ARGV) {
  @ARGV = grep { $_ !~ /^--trad$/} @ARGV;
  $TRAD = 1;
}
print_help() unless @ARGV;
print @ARGV, "\n";
#All specials args and args error are be checked, we can handle file
for my $file (@ARGV) {
  my ($name, $path, $suffix) = fileparse($file, '\..*$');
  if ($suffix eq '.ini') {
    $KARAOKE_DIR |= $path;
    print "Play $file\n";
    play_ini ($file);
  } else {
    open PLS, '<', $file or toy_die "Can't open $file : $!";
    play_ini($_) while (<PLS>);
    close PLS;
  }
}
exit 0;
#Parse and play the .ini file
sub play_ini {
  my $file = shift;
  my %info;
  infos "Ouverture de $file";
  open INI, '<', $file or toy_die "Can't open file $file :$!";
  while (<INI>) {
    my ($key, $value) = $_ =~ /^(.+?)=(.+)/;
    $value =~ s/\s+$//g if $value;
    $info{$key} = $value if $key and $value;
  }
  $info{video} = catfile($VIDEOS_DIR, $info{aviname});
  $info{sub}   = catfile($LYRICS_DIR, $info{file});
  infos "Entering $KARAOKE_DIR directory";
  chdir($KARAOKE_DIR);
  #Needed options, fail if not found
  if (-r $info{video}) {
    infos "Video file is : $info{aviname}";
  } else {
    toy_die "Can't read the video file $info{video}"; 
  }
  if (-r $info{sub}) {
    infos "Subtitle file is : $info{file}";
  } else {
    toy_die "Can't read the lyrics file $info{sub}";
  }
  #Optionnals options, just warn if not found
  if (defined $info{logo}) {
    $info{logof} = catfile($LOGOS_DIR, $info{logo});
    if (-r $info{logof}) {
      infos "Logo file is : $info{logo}";
    } else {
      warn "Can't read the logo file : $info{logof}, use the default logo";
      delete $info{logo}; delete $info{logof};
    }
  }
  if ($TRAD and defined $info{traduction}) {
    $info{trad} = catfile($TRADS_DIR, $info{traduction});
    if (-r $info{trad}) {
      infos "Traduction file is : $info{traduction}";
      my ($fh, $filename) = tempfile();
      open SUB, $info{sub};
      open TRAD, $info{trad};
      print $fh $_ for (<SUB>);
      print $fh $_ for (<TRAD>);
      close SUB;
      close TRAD;
      close $fh;
      $info{sub} = $filename;
    } else {
      warn "Can't read the traduction file : $info{trad}, don't use traduction file";
      delete $info{trad}; delete $info{traduction};
    }
  }
  #Launch the toyunda player
  my ($fh, $tmpfilename) = tempfile();
  close $fh;
  open my $oldstdout, '>&STDOUT';
  open my $oldstderr, '>&STDERR';
  open STDOUT, '>', $tmpfilename;
  open STDERR, '>', $tmpfilename;;
  if (defined $info{logo}) {
    system $TOYUNDA_PLAYER, '-fs', '-logo', $info{logof}, '-sub', $info{sub}, $info{video};
  } else {
    system $TOYUNDA_PLAYER, '-fs', '-sub', $info{sub}, $info{video};
  }
  open STDOUT, '>&', $oldstdout;
  open STDERR, '>&', $oldstderr;
}


