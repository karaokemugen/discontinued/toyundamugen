##
## variables.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Tue Aug 24 20:41:54 2004 Olivier Leclant
## $Id: variables.rb 1.6 Tue, 02 Nov 2004 19:29:51 +0100 olivier $
##
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

#
# type-safe dynamically-declared variables
#

# I just learned today that a kind of similar class is defined in the
# standard library (ostruct.rb)

class Variables

  class Error < Exception; end

  def initialize
    @variables = {}
  end

  def declared?(var_name)
    not @variables[var_name].nil?
  end

  def declare(var_name, value)
    raise Error, "var `#{var_name}' declared twice" if declared? var_name
    raise Error, "invalid value class" unless value.respond_to? "value" and value.respond_to? "value="
    @variables[var_name] = value
  end

  def [](var_name)
    return @variables[var_name].value if declared? var_name
    raise Error, "variable `#{var_name}' is not declared."
  end

  def []=(var_name, value)
    return (@variables[var_name].value = value) if declared? var_name
    raise Error, "variable `#{var_name}' is not declared."
  end

  def reset(var_name)
    @variables[var_name].reset
  end

  def set_default(var_name, value)
    @variables[var_name].default = value
  end

  def to_s
    @variables.sort.collect { |name, var| var.to_s(name) }.flatten.to_s
  end

  def deep_clone
    Marshal.load(Marshal.dump(self))
  end

  def method_missing(id, *args)
    n = id.id2name
    if n.match(/^(.*)=$/)
      return (self[$1] = args[0]) if args.size == 1
    else
      return self[n] if args.empty?
    end
    raise Error, "internal error in Variables::method_missing for #{n}"
  end

end
