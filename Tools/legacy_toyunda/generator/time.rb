#!/usr/bin/ruby -w

require 'aviparse-timeonly'

def read_ini(filename)
  mo = 0
  directory = ""
  aviname = ""
  begin
    f = File.new(filename.chomp!)
    f.each_line do |line|
      mo = 1 if (line =~ /\[MOVIE\]/ and mo == 0)
#      if (mo == 1 and line =~ /directory\s*=\s*(.*)/) then directory = $1 end
      if (mo == 1 and line =~ /aviname\s*=\s*(.*)/) then aviname = $1 end
    end
    directory = "videos" if directory == ""
  ensure
    return directory + '/' + aviname.chomp
  end
end

RUBIX = true
if RUBIX then
  
  duration = 0
  ARGV.each do |video|
    if (video =~ /.avi$/)
      a = AviFile.new(video)
      unless a.invalid then
        d = a.duration
        print "#{video} : #{d} seconds\n"
        duration += a.duration
      end
    end
  end
  hours = duration / 3600
  duration %= 3600
  print hours, " hours, ", duration / 60, " minutes and ", duration % 60, " seconds\n"
  exit
end


duration = 0
while gets
  video = read_ini($_)
  if (video =~ /.avi$/)
    a = AviFile.new(video)
    d = a.duration
#    print "#{video} : #{d} seconds\n"
    duration += a.duration
  end
end
hours = duration / 3600
duration %= 3600
print hours, " hours, ", duration / 60, " minutes and ", duration % 60, " seconds\n"
