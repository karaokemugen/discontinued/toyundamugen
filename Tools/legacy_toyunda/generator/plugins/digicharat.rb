##
## unfold.rb.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Aug 27 18:13:58 2004 Olivier Leclant
## $Id: unfold.rb 1.1 Mon, 27 Sep 2004 11:15:36 +0200 olivier $
##
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

module ToyundaGen
  module Styles
    module SupportedStyles
      class Dejiko < Style
        def process_line(writer, line)
         writer.add(line.frames, line.colors[0], line, " - nyo") 
        end
      end
      class Puchiko < Style
        def process_line(writer, line)
         writer.add(line.frames, line.colors[0], line, " - nyu") 
        end
      end
      class Gema < Style
        def process_line(writer, line)
         writer.add(line.frames, line.colors[0], line, " - gema") 
        end
      end
      class Puyoko < Style
        def process_line(writer, line)
         writer.add(line.frames, line.colors[0], line, " - pyo") 
        end
      end
    end
  end
end
