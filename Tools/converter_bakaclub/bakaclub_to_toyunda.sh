LISTE=$1.txt
while read KARA
do
	INIFILE=""
	VIDEO=`echo $KARA | awk -F\| {'print $1'}`
	INIFILE=`echo $KARA | awk -F\| {'print $2'}`
	if [ "$INIFILE" == "" ] 
	then
		INIFILE=${VIDEO%.*}
	fi 
	INIFILE="new/ini/JAP - $INIFILE.ini"
	cp "$VIDEO" new/Videos/
	BASEVIDEO=`basename "$VIDEO"`
	echo "origin=Bakaclub" >"$INIFILE"
	echo "[MOVIE]" >>"$INIFILE"
	echo "directory=Videos" >>"$INIFILE"
	echo "aviname=$BASEVIDEO" >>"$INIFILE"
	echo "[SUBTITLES]" >>"$INIFILE"
	echo "directory=Lyrics" >>"$INIFILE"
	echo "file=dummy.ass" >>"$INIFILE"
done < $LISTE
