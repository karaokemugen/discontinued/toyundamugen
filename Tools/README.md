# Utilitaires Toyunda Mugen

Ce dossier contient notamment :

- Un int�grateur de karaoke Bakaclub vers Toyunda Mugen
- Un int�grateur de karaoke de Karaoke.moe vers Toyunda Mugen
- Les outils (anciens) de time de Toyunda d'Epitanime
- Le code de la page web de http://mei.do/toyundamugen qui permet d'envoyer � quelqu'un ses propres karaokes au format toyunda mugen directement.
	- Contient un convertisseur (convert_base_to_ass.sh) qui convertira une ancienne base Toyunda (.txt) en fichiers ass avec l'aide de :
		- Toyundagen2ass pour les times Toyunda v3
		- EpitASS pour les times Toyunda v1 et v2 (par Seipas : https://framagit.org/Seipas/EpitASS )

Pour les int�grateurs, merci de lire le code source pour comprendre comment ils doivent �tre utilis�s. Ils seront comment�s un autre jour :)

