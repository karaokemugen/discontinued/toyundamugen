<?php
 
//A MODIFIER :



//  https://lab.shelter.moe/toyundamugen/times/issues/3
//  https://lab.shelter.moe/toyundamugen/times/blob/master/CONTRIBUTING.md


define('CFG_UPLOAD_LOCAL_DIR','/volume1/web/toyunda-mugen/uploaded/');
define('CFG_KARAOKE_LIST','/volume1/web/toyunda-mugen/ini');



define('CFG_EMAIL','axel@teri-chan.net');


 
 
define('CFG_EXTENSION_VIDEO','avi,mkv,mp4,webm');
define('CFG_EXTENSION_SOUSTITRES','txt,srt,vtt,ass');
define('CFG_EMAIL_SUJET','[Toyunda-Mugen] Nouveau kara par %s');
define('CFG_EMAIL_CONTENU','Karas ajoutés:<br><br>%s<br><br>%s');
define('CFG_NOMBRE_BLOCK_KARAOKE',100);



$langue_cetegorie=array(
	'JAP'=>'Japonais',
	'ANG'=>'Anglais',
	'FR'=>'Français',
	'RUS'=>'Russe',
	'ALL'=>'Allemand',
	'ESP'=>'Espagnol',
	'CHI'=>'Chinois',
	'LAT'=>'Latin',
    'ITA'=>'Italien',
	'COR'=>'Corréen',
	'SUE'=>'Suédois',
	'POR'=>'Portugais',
	'FIC'=>'Fictionnel',
	'HEB'=>'Hébreu',
	'EPO'=>'Esperanto',
	'JPOP'=>'J-Pop'
);

$type=array(
	'OP'=>'Opening',
	'ED'=>'Ending',
	'IN'=>'Insert Song',
	'OAV OP'=>'Opening d\'OAV',
	'OAV ED'=>'Ending d\'OAV',
	'OP LIVE'=>'Opening d\'animé version concert',
	'OT'=>'Autre / inclassable',
	'AMV'=>'AMV',
	'MOVIE OP'=>'Opening de film d\'animation',
	'MOVIE ED'=>'Ending de film d\'animation',
	'MOVIE IN'=>'Insert Song de film d\'animation',
	'MOVIE IN LIVE'=>'Insert Song d\'un film avec des vrais gens',
	'GAME OP'=>'Opening de JV',
	'GAME ED'=>'Ending de JV',
	'GAME IN'=>'Game Insert Song',
	'GAME AMV'=>'Clip musical de jeu vidéo',
	'GAME OT'=>'"Autre" de JV',
	'GAME PV'=>'Vidéo promotionnelle de jeu vidéo (trailer, bande annonce)',
	'R18 GAME OP'=>'Opening de JV interdit au moins de 18 ans',
	'TOKU OP'=>'Opening de tokusatsu / sentai',
	'VOCA'=>'Chanson Vocaloid',
	'VOCA AMV'=>'AMV d\'une série avec chanson Vocaloid',
	'IN LIVE'=>'Insert Song de série TV live',
	'LIVE'=>'Concert',
	'CLIP'=>'Clip musical',
	'PV'=>'Pub/Promotional',
	'CM'=>'Pub'
);




// Conversion VTT=>SRT
/////////////////////////////////
function subtitle_convert_vtt_to_srt($contenu) {
	$contenu=to_unix($contenu);
	// retrait WEBVTT header
	$contenu=explode("\n",$contenu);
	foreach($contenu as $numline => $valline) {
		if(trim($valline)=='WEBVTT') {
			$contenu[$numline]='';
		}
	}
	$contenu=implode("\n",$contenu);
	$contenu=trim($contenu);
	
	//conversion des . en , et renumérotation des sous-titres
	$contenu=explode("\n",$contenu);
	$i=0;
	foreach($contenu as $numline => $valline) {
		if(!isset($contenu[$numline-1]) || trim($contenu[$numline-1])=='') {
			$contenu[$numline]=++$i."\n".$contenu[$numline];
		}
		if(strpos($valline,' --> ')!==false) {
			$contenu[$numline]=str_replace('.',',',$contenu[$numline]);
			$data=explode(' ',$contenu[$numline]);
			$contenu[$numline]=$data[0].' --> '.$data[2];
		}
	}
	return implode("\n",$contenu);
}
	
	
	
// Conversion SRT=>ASS
/////////////////////////////////
function subtitle_convert_srt_to_ass($contenu) {
	//passage au format ASS
	$contenu2=explode("\n",$contenu);
	$deb=0;
	$time_deb='';
	$time_fin='';
	$cont=array();
	$out=array();
	foreach($contenu2 as $numline => $valline) {
		if(strpos($valline,' --> ')!==false) {
			$deb=1;
			$time=explode(' --> ',$valline);
			$time_deb=trim(str_replace(',','.',$time[0]));
			$time_fin=trim(str_replace(',','.',$time[1]));
			$time_deb=substr($time_deb,0,-1);
			$time_fin=substr($time_fin,0,-1);
			continue;
		}
		if($deb==1) {
			if(trim($valline)!='') {
				$cont[]=trim($valline);
			} else {
				$out[]='Dialogue: 1,'.$time_deb.','.$time_fin.',DefaultVCD, NTP,0000,0000,0000,,{\an8}'.implode('\N',$cont);
				$deb=0;
				$time_deb='';
				$time_fin='';
				$cont=array();
			}
		}
	}
	$contenu2='[Script Info]
Title: soustitre
ScriptType: v4.00+
Collisions: Reverse

[V4+ Styles]
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding
Style: DefaultVCD,Arial,20,&H00FFFFFF,&H000088EF,&H00000000,&H00666666,-1,0,0,0,100,100,0,0,1,3,0,8,10,10,10,1

[Events]
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text
';
	$contenu2.=implode("\n",$out);
	return $contenu2;
}



// Fonctions utilitaires
/////////////////////////////////
function Getunits($bytes) {
  if ($bytes<=0) return '0';
  $s=array('o', 'Ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo');
  $e=floor(log($bytes,1024));
  return round($bytes/pow(1024,$e),2).''.$s[$e];
}

function return_bytes($val) {
    switch (substr ($val, -1)) {
        case 'K': case 'k': return (int)$val * 1024;
        case 'M': case 'm': return (int)$val * 1048576;
        case 'G': case 'g': return (int)$val * 1073741824;
        default: return $val;
    }
}

function make_dir_recursif($dir) {
	if(!is_dir($dir)) {		mkdir($dir, 0777, true);	}
}

function valid_field($type, $data) {
	// check regexp: http://regex101.com/
	switch ($type) {
		case 'alphanumtirets':return preg_match('`^[a-zA-Z0-9_\-]+$`',$data); // 
		case 'integer':return preg_match('`^[\-0-9]+$`',$data); // 
		case 'float':return preg_match('`^[\-0-9\.\,]+$`',$data); // 
		case 'email':return preg_match('`^[^@]+@[^@]+$`',$data); // http://davidcel.is/blog/2012/09/06/stop-validating-email-addresses-with-regex/
		case 'tel':return preg_match('`^[0-9\+\(\)\. ]+$`',$data);
		case 'code_postal':return preg_match('`^[0-9a-zA-Z- ]+$`',$data); // http://en.wikipedia.org/wiki/List_of_postal_codes
		case 'http_and_https':return preg_match('`^https?\:\/\/.+$`',$data); // http://en.wikipedia.org/wiki/List_of_postal_codes
	}
}



// Test des droits d'accès
/////////////////////////////////
if( !is_dir(CFG_UPLOAD_LOCAL_DIR) && !mkdir(CFG_UPLOAD_LOCAL_DIR,0777,true) ) {
	die('erreur de droit d\'accès apache, impossible de creer le dossier '.CFG_UPLOAD_LOCAL_DIR);
}
if (!touch(CFG_UPLOAD_LOCAL_DIR.'testdroitaccess')) {
	die('erreur de droit d\'accès apache, impossible d\'écrire dans '.CFG_UPLOAD_LOCAL_DIR);
}
unlink(CFG_UPLOAD_LOCAL_DIR.'testdroitaccess');


// Récuperation des informations
/////////////////////////////////
$ip=$_SERVER["REMOTE_ADDR"];
$host=gethostbyaddr($ip);
$sizes=array(
    return_bytes(ini_get('post_max_size')),
    return_bytes(ini_get('upload_max_filesize')),
    return_bytes(ini_get('memory_limit'))
);    


// Fichiers soumis
/////////////////////////////////
if(!empty($_POST['pseudo'])) {
	
	//sauvegarde du pseudo dans un cookie
	$pseudo=$_POST['pseudo'];
	if(!valid_field('alphanumtirets',$pseudo)) die('Pseudo invalide');
	setcookie('uploadaxel_pseudo',$pseudo,time()+3600*24*365);
	
	
	//récuperation des variables
	$id=(int)$_POST['id'];
	$langue=str_replace('/','_',$_POST['langue_'.$id]);
	$type=str_replace('/','_',$_POST['type_'.$id].$_POST['ordre_'.$id]);
	$serie=str_replace('/','_',$_POST['serie_'.$id]);
	$titre=str_replace('/','_',$_POST['titre_'.$id]);
	$doublon=(empty($_POST['doublon_ckbox_'.$id]))?'':$_POST['doublon_'.$id];
	
	//details (extensions&co) des videos et soustitres
	$sosutitre_filename=$_FILES['fichier_soustitre_'.$id]['name'];
	$sosutitre_ext=pathinfo($sosutitre_filename,PATHINFO_EXTENSION);
	$sosutitre_content=file_get_contents($_FILES['fichier_soustitre_'.$id]['tmp_name']);
	
	$video_filename=$_FILES['fichier_video_'.$id]['name'];
	$video_ext=pathinfo($video_filename,PATHINFO_EXTENSION);
	$video_path=$_FILES['fichier_video_'.$id]['tmp_name'];
	
	//restrictions d'extensions de fichiers
	$ext_autorisees_videos=explode(',',CFG_EXTENSION_VIDEO);
	$ext_autorisees_soustitres=explode(',',CFG_EXTENSION_SOUSTITRES);
	if( !in_array($video_ext,$ext_autorisees_videos) )  die('extension de vidéo invalide');
	
	$soustitre_requis=true;
	if( !in_array($sosutitre_ext,$ext_autorisees_soustitres)) {
		if($video_ext=='mkv') {
			$soustitre_requis=false;
		} else {
			die('extension de sous-titre invalide');
		}
	}
	
	$nomkara=$langue.' - '.$serie.' - '.$type.' - '.$titre;
	
	//création des chemins et répertoires
	$dir_doublon=CFG_UPLOAD_LOCAL_DIR.$pseudo.'/doublon';
	$dir_nouveau=CFG_UPLOAD_LOCAL_DIR.$pseudo.'/nouveau';
	make_dir_recursif(CFG_UPLOAD_LOCAL_DIR.$pseudo);
	if($doublon) {
		$storedir=$dir_doublon;
		make_dir_recursif($storedir);
		//enregisrement de la raison du doublon
		file_put_contents($storedir.'/raison_'.$nomkara.'.txt',$doublon);
	} else {
		$storedir=$dir_nouveau;
		make_dir_recursif($storedir);
	}
	make_dir_recursif($storedir.'/videos');
	make_dir_recursif($storedir.'/lyrics');
	make_dir_recursif($storedir.'/karas');
	
	//copie Video
	$video_dest=$storedir.'/videos/'.$nomkara.'.'.$video_ext;
	move_uploaded_file($video_path,$video_dest);
	
	//copie soustitres
	if($sosutitre_ext=='vtt') {
		$sosutitre_content=subtitle_convert_vtt_to_srt(subtitle_convert_srt_to_ass($sosutitre_content));
		$sosutitre_ext='ass';
	}
	if($sosutitre_ext=='srt') {
		$sosutitre_content=subtitle_convert_srt_to_ass($sosutitre_content);
		$sosutitre_ext='ass';
	}
	$soustitre_dest=$storedir.'/lyrics/'.$nomkara.'.'.$sosutitre_ext;
	file_put_contents($soustitre_dest,$sosutitre_content);
	
	//création du .kara
	$ini_dest=$storedir.'/karas/'.$nomkara.'.kara';
	$ini_content.='videofile=\"'.$nomkara.'.'.$video_ext."\"\n";
	$ini_content.='subfile=\"'.$nomkara.'.'.$sosutitre_ext."\"\n";
	
	
	
	
	$ini_content.='year=\"'.$year."\"\n";
	$ini_content.='singer=\"'.$singer."\"\n";
	$ini_content.='tags=\"'.$tags."\"\n";
	$ini_content.='songwriter=\"'.$songwriter."\"\n";
	$ini_content.='additional_languages=\"'.$additional_languages."\"\n";
	$ini_content.='creator=\"'.$creator."\"\n";
	$ini_content.='author=\"'.$author."\"\n";
	
	
	
	
	
	
	
	$year=str_replace('/','_',$_POST['year_'.$id]);
	$singer=str_replace('/','_',$_POST['singer_'.$id]);
	$tags=str_replace('/','_',$_POST['tags_'.$id]);
	$songwriter=str_replace('/','_',$_POST['compositeur_'.$id]);
	$additional_languages=str_replace('/','_',$_POST['languesupp_'.$id]);
	$creator=str_replace('/','_',$_POST['studio_'.$id]);
	$author=str_replace('/','_',$_POST['origauthor_'.$id]);
	
	/*
	
year=""
singer=""
tags=""
songwriter=""
additional_languages=""
creator=""
author="Bakaclub"kara*/
	

	file_put_contents($ini_dest,$ini_content);


	
	//envoi du mail
	$sujet=sprintf(CFG_EMAIL_SUJET,htmlspecialchars($pseudo));
    $texte= 'IP:'.$ip." - ".
            'HOST:'.$host." - ".
            'DATE:'.date('H:i:s d/m/Y')."<br>".
            '======================'."<br>";
    $doublontext=$doublon?'Doublon. Raison:<br>'.nl2br(htmlspecialchars($doublon)):'';
    $texte.=sprintf(CFG_EMAIL_CONTENU,htmlspecialchars($nomkara),htmlspecialchars($doublontext));
    
    //headers
    $header =
	'From: '.CFG_EMAIL.PHP_EOL.
	'Reply-To: '.CFG_EMAIL.PHP_EOL.
	'X-Mailer: PHP'.phpversion().PHP_EOL.
	'MIME-Version: 1.0'.PHP_EOL.
	'Content-Type: text/html; charset=UTF-8'.PHP_EOL.
	'Content-Transfer-Encoding: 8bit';
    //body
    $body  = $texte;
    
    mail(CFG_EMAIL,'=?UTF-8?B?'.base64_encode($sujet).'?=',$body,$header);
	
	die();
}

	
	
	

function mail_txt_utf8($exp,$dest,$sujet,$contenu) {
	mail($dest,'=?UTF-8?B?'.base64_encode($sujet).'?=',$contenu,
	'From: '.$exp.PHP_EOL.
	'Reply-To: '.$exp.PHP_EOL.
	'X-Mailer: PHP'.phpversion().PHP_EOL.
	'MIME-Version: 1.0'.PHP_EOL.
	'Content-Type: text/plain; charset=UTF-8'.PHP_EOL.
	'Content-Transfer-Encoding: 8bit');
}









//récuperation de la liste des kara existants pour l'autocomplete
$listfiles=array_diff(scandir(CFG_KARAOKE_LIST), array('..', '.'));
$listdir_detectdoublon=array();
$listdir_langue_cetegorie=array();
$listdir_serie=array();
$listdir_type=array();
$listdir_titre=array();
foreach($listfiles as $file) {
	$file=str_replace('.ini','',$file);
	$file=explode(' - ',$file);
	if(!empty($file[0])) $listdir_langue_cetegorie[]=$file[0];
	if(!empty($file[1])) $listdir_serie[]=$file[1];
	if(!empty($file[2])) $listdir_type[]=$file[2];
	if(!empty($file[3])) $listdir_titre[]=$file[3];
	if(!empty($file[1]) && !empty($file[3])) $listdir_detectdoublon[]=$file[1].$file[3];
}
$listdir_langue_cetegorie=array_unique($listdir_langue_cetegorie);
$listdir_serie=array_unique($listdir_serie);
$listdir_type=array_unique($listdir_type);
$listdir_titre=array_unique($listdir_titre);



// echo '<pre>'.print_r($listdir_langue_cetegorie,true).'</pre>';
// echo '<pre>'.print_r($listdir_type,true).'</pre>';


// Affichage de la page
/////////////////////////////////
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Upload de karas</title>
    </head>
	<style>
	.bloc_kara {
		border:1px solid grey;
		margin-bottom:1.3rem;
		padding:0.3rem;
	}
	</style>
    <body>
    
			<p title="Limitations dûes à la configuration de PHP&nbsp;:&#013;post_max_size&nbsp;: <?php echo Getunits($sizes[0]) ?>&#013;upload_max_filesize&nbsp;: <?php echo Getunits($sizes[1])?>&#013;memory_limit&nbsp;: <?php echo Getunits($sizes[2])?>">(Taille max des vidéos&nbsp;: <?php echo Getunits(min($sizes))?>)</p>
        
            <p>Pseudo : <input type="text" id="pseudo" name="pseudo" required pattern="[a-zA-Z0-9_\-]+" value="<?php echo !empty($_COOKIE['uploadaxel_pseudo'])?$_COOKIE['uploadaxel_pseudo']:''; ?>"> (caractères alphanumériques et "_-" uniquement) </p> 
			
			
			<p>Nombre de Karaokés à uploader : <select id="nombre_karas" onchange="nombrekara(this)">
				<?php for($i=1;$i<=CFG_NOMBRE_BLOCK_KARAOKE;++$i) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select></p>
			
			<?php for($i=1;$i<=CFG_NOMBRE_BLOCK_KARAOKE;++$i) { ?>
				
			
			<form id="form_<?php echo $i; ?>" class="bloc_kara" method="POST" enctype="multipart/form-data" style="<?php echo ($i!=1)?'display:none;':''; ?>">
			
			<input type="hidden" id="sent_<?php echo $i; ?>" value="0">
			
			<table>
			
			<tr>
				<td>Vidéo: </td>
				<td><input type="file" id="fichier_video_<?php echo $i; ?>" name="fichier_video_<?php echo $i; ?>" onchange="return video(this)"><span class="info"></span></td>
				<td>Sous-titre: </td>
				<td><input type="file" id="fichier_soustitre_<?php echo $i; ?>" name="fichier_soustitre_<?php echo $i; ?>" onchange="return soustitre(this)"><span class="info"></span></td>
			</tr>
			
			
		
			<tr>
				<td>Langue: </td>
				<td><select  name="langue_<?php echo $i; ?>" id="langue_<?php echo $i; ?>">
				<option value="" hidden>Choisir</option>
				<?php foreach($langue_cetegorie as $key => $val) { ?>
				<option value="<?php echo $key; ?>"><?php echo $val; ?></option>
				<?php } ?>
			</select></td>
			
			
			
			</tr>
			
			<tr>
			<tr>
				<td>Langue supplémentaire: </td>
				<td colspan="3"><input type="text" id="languesupp_<?php echo $i; ?>" name="languesupp_<?php echo $i; ?>"  style="width:30em;"></td>
			</tr>
			
			</tr>
			<tr>
				<td>Type: </td>
				<td><select id="type_<?php echo $i; ?>" name="type_<?php echo $i; ?>">
				<option value="" hidden>Choisir</option>
				<?php foreach($type as $key => $val) { ?>
				<option value="<?php echo $key; ?>"><?php echo $val; ?></option>
				<?php } ?>
			</select></td>
			
			
				<td>Ordre: </td>
				<td><select name="ordre_<?php echo $i; ?>" id="ordre_<?php echo $i; ?>">
				<option value="">(aucun)</option>
				<?php
				$ordre=range(0,99);
				foreach($ordre as $val) { ?>
				<option value="<?php echo $val; ?>"><?php echo $val; ?></option>
				<?php } ?>
			</tr>
			
			
			<tr>
				<td>Titre de l'animé/série/groupe: </td>
				<td colspan="3"><input type="text" id="serie_<?php echo $i; ?>" name="serie_<?php echo $i; ?>"  onkeyup="listeseries_autosuggest(this,<?php echo $i; ?>)" style="width:30em;"><span id="auto_serie_<?php echo $i; ?>" style="display:none;"></span></td>
			</tr>
			
			<tr>
				<td>Titre de la chanson: </td>
				<td colspan="3"><input type="text" id="titre_<?php echo $i; ?>" name="titre_<?php echo $i; ?>" onkeyup="listetitres_autosuggest(this,<?php echo $i; ?>)" style="width:30em;"><span id="auto_titre_<?php echo $i; ?>" style="display:none;"></span></td>
			</tr>
			
			
			
			
			<tr>
				<td>Année: </td>
				<td colspan="3"><input type="text" id="year_<?php echo $i; ?>" name="year_<?php echo $i; ?>"  style="width:30em;"><span id="auto_titre_<?php echo $i; ?>" style="display:none;"></span></td>
			</tr>
			
			
			
			<tr>
				<td>Chanteur: </td>
				<td colspan="3"><input type="text" id="singer_<?php echo $i; ?>" name="singer_<?php echo $i; ?>" style="width:30em;"><span id="auto_titre_<?php echo $i; ?>" style="display:none;"></span></td>
			</tr>
			
			
			<tr>
				<td>Tags: </td>
				<td colspan="3"><input type="text" id="tags_<?php echo $i; ?>" name="tags_<?php echo $i; ?>" style="width:30em;"><span id="auto_titre_<?php echo $i; ?>" style="display:none;"></span></td>
			</tr>
			
			
			<tr>
				<td>Compositeur/parolier: </td>
				<td colspan="3"><input type="text" id="compositeur_<?php echo $i; ?>" name="compositeur_<?php echo $i; ?>"  style="width:30em;"><span id="auto_titre_<?php echo $i; ?>" style="display:none;"></span></td>
			</tr>
			
			
			<tr>
				<td>Studio d'animation/origine: </td>
				<td colspan="3"><input type="text" id="studio_<?php echo $i; ?>" name="studio_<?php echo $i; ?>"  style="width:30em;"><span id="auto_titre_<?php echo $i; ?>" style="display:none;"></span></td>
			</tr>
			
			
			<tr>
				<td>Auteur: </td>
				<td colspan="3"><input type="text" id="origauthor_<?php echo $i; ?>" name="origauthor_<?php echo $i; ?>"  style="width:30em;"><span id="auto_titre_<?php echo $i; ?>" style="display:none;"></span></td>
			</tr>
			
			
			
			<tr>
				<td>Progression de l'upload: </td>
				<td id="progression" colspan="3"><progress id="avancement_<?php echo $i; ?>" value="0" max="100" style="width:300px;"></progress><span id="pourcentage_<?php echo $i; ?>"></span></td>
			</tr>
			
			
			</table>
			
			
			
			</form>
			<?php } ?>
			<div>
				<input type="button" onclick="send_button();" value="Envoyer">
			</div>
        </form>
    </body>

    <script>
CFG_EXTENSION_VIDEO='<?php echo CFG_EXTENSION_VIDEO; ?>'.split(',');
CFG_EXTENSION_SOUSTITRES='<?php echo CFG_EXTENSION_SOUSTITRES; ?>'.split(',');

//listes pour l'autosuggest

var listeseries=[
	<?php foreach($listdir_serie as $serie) {	echo '\''.addslashes($serie).'\',';}?>
];
var listetitres=[
	<?php foreach($listdir_titre as $serie) {	echo '\''.addslashes($serie).'\',';}?>
];

//gestion de l'autosuggest, affichage du menu deroulant de choix contenant de résultat de ordered_match_list correspondant au mot tapé
function listeseries_autosuggest(myele) {
	myele.style.display='';
	myele.setAttribute('autocomplete', 'off');
	myele.setAttribute('onfocus', 'document.getElementById("auto_'+myele.id+'").style.display="";');
	myele.setAttribute('onblur', 'setTimeout(function(){document.getElementById("auto_'+myele.id+'").style.display="none";},200);');
	
	sp3=document.getElementById('auto_'+myele.id);
	
	sp3.setAttribute('style', 'min-width:'+myele.offsetWidth+'px;margin-top:'+myele.offsetHeight+'px;margin-left:-'+myele.offsetWidth+'px;position:absolute;border:1px solid grey;z-index:9999;text-align:left;background:white;max-height:130px;overflow-y:auto;overflow-x: hidden;');
	
	var outhtml='';
	out=ordered_match_list(myele.value,listeseries);
	for (i in out) {
		outhtml+='<span onclick="document.getElementById(\''+myele.id+'\').value=\''+out[i][1]+'\'">'+out[i][1]+'</span><br>';
	}
	sp3.innerHTML=outhtml;
}

function listetitres_autosuggest(myele,num) {
	myele.style.display='';
	myele.setAttribute('autocomplete', 'off');
	myele.setAttribute('onfocus', 'document.getElementById("auto_'+myele.id+'").style.display="";');
	myele.setAttribute('onblur', 'setTimeout(function(){document.getElementById("auto_'+myele.id+'").style.display="none";},200);');
	
	sp3=document.getElementById('auto_'+myele.id);
	
	sp3.setAttribute('style', 'min-width:'+myele.offsetWidth+'px;margin-top:'+myele.offsetHeight+'px;margin-left:-'+myele.offsetWidth+'px;position:absolute;border:1px solid grey;z-index:9999;text-align:left;background:white;max-height:130px;overflow-y:auto;overflow-x: hidden;');
	
	var outhtml='';
	out=ordered_match_list(myele.value,listetitres);
	for (i in out) {
		outhtml+='<span onclick="document.getElementById(\''+myele.id+'\').value=\''+out[i][1].replace(/'/g, "&#039;")+'\';">'+out[i][1]+'</span><br>';
	}
	sp3.innerHTML=outhtml;
}


// fonction qui affiche la liste "list" trié par pertinance de "search" 
// search = le /les mots à chercher dans la phrase
// list = tableau de phrases dans lesquels chercher
// tableau de retour trié par meilleur score
function ordered_match_list(search,list) {
	var maxscore=0;
	var out=[];
	for (i in list) {
		var list_ele=list[i];
		score=kmatch2(search, list_ele);
		out.push([score,list_ele]);
		maxscore=(score>maxscore)?score:maxscore;
	}
	out.sort(function(a, b) {
		if (a[0] === b[0]) {	return 0;	}
		else {	return (a[0] < b[0]) ? -1 : 1;	}
	});
	out.reverse();
	
	//on limite la sortie
	var out_limited=[];
	maxresult=15;
	if(search.length==0 || search.length==1) maxresult=0;
	for(i in out) {
		if(maxresult && out[i][0]/maxscore > 0.5) {
			--maxresult;
			out_limited.push(out[i]);
		}
	}
	
	return out_limited;
}
// calcul le score d'un terme calculé par le nombre de sous-chaines de s1
// se retrouvant dans s2 majoré par la taille de la sous-chaine
function kmatch2(s1, s2) {
	s1=s1.toLowerCase();
	s2=s2.toLowerCase();
    var l1=s1.length;
    var l2=s2.length;
	var found=0;
	var score=0;
	for(i=0;i<l2;++i) {
		found=0;
		for(j=0;j<l1;++j) {
			if(s1.charAt(j) == s2.charAt(i+j)) {
				found+=found?found*2:1;
				score+=found-1;
			}
		}
	}
	return score;
}

		
		
		
		

//desacrtivation d'un kara si l'upload s'est bien passé (ou à merdé)
function disable_kara_uploaded(num) {
	var formele=document.getElementById('form_'+num);
	formele.style.backgroundColor='#eeeeee';
	inputlist=formele.getElementsByTagName('input');
	for (i in inputlist) {
		inputlist[i].disabled=true;
	}
	inputlist=formele.getElementsByTagName('select');
	for (i in inputlist) {
		inputlist[i].disabled=true;
	}
	inputlist=formele.getElementsByTagName('textarea');
	for (i in inputlist) {
		inputlist[i].disabled=true;
	}
}



//changement du nbr de blocks
function nombrekara(ele) {
	for (i=1; i<=<?php echo CFG_NOMBRE_BLOCK_KARAOKE; ?> ; ++i ) {
		document.getElementById('form_'+i).style.display=((ele.value>=i)?'':'none');
	}
}

function Getunits(bytes) {
   if (bytes<=0) return '0';
   multiple=1024;
   s=['octets', 'Ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo'];
   e=Math.floor(Math.log(bytes)/Math.log(multiple));
   return ( Math.round( bytes/Math.pow(multiple,e)*100) /100 ) +''+s[e];
}

function video(file_ele) {
	bloc=file_ele.parentNode.getElementsByClassName("info");
	bloc[0].innerHTML='('+Getunits(file_ele.files[0].size)+')';
	extension=file_ele.files[0].name.split('.').pop();
	if(CFG_EXTENSION_VIDEO.indexOf(extension)==-1) {
		file_ele.value=null;
		bloc[0].innerHTML='';
		alert('Extension "'+extension+'" non supportée.');
	}
}
function soustitre(file_ele) {
	bloc=file_ele.parentNode.getElementsByClassName("info");
	bloc[0].innerHTML='('+Getunits(file_ele.files[0].size)+')';
	extension=file_ele.files[0].name.split('.').pop();
	if(CFG_EXTENSION_SOUSTITRES.indexOf(extension)==-1) {
		file_ele.value=null;
		bloc[0].innerHTML='';
		alert('Extension "'+extension+'" non supportée.');
	}
	
}

//formulaire soumis
var current_form;
var current_form;
function send_button() {
	for (i=1; i<=<?php echo CFG_NOMBRE_BLOCK_KARAOKE; ?> ; ++i ) {
		if(document.getElementById('nombre_karas').value >= i && document.getElementById('sent_'+i).value==0) {
			
			console.log('currentform:'+i);
			current_form=i;
			if(!document.getElementById('pseudo').value) {
				alert('Nom requis');return;
			}
			if(!document.getElementById('fichier_video_'+i).value != '') {
				alert('Vidéo requise');return;
			}
			console.log('fichier_video_'+i);
			if(!document.getElementById('fichier_soustitre_'+i).value != '') {
				extension=document.getElementById('fichier_video_'+i).value.split('.').pop();
				if(extension!='mkv') {
					alert('sous-titre requis');return;
				}
			}
			if(!document.getElementById('serie_'+i).value) {
				alert('Titre d\'anime/série/groupe requis');return;
			}
			if(!document.getElementById('titre_'+i).value) {
				alert('Titre de la chanson requis');return;
			}
			if(!document.getElementById('langue_'+i).value) {
				alert('Langue/catégorie requis');return;
			}
			if(!document.getElementById('type_'+i).value) {
				alert('Type requis');return;
			}
			
		}
	}
	for (i=1; i<=<?php echo CFG_NOMBRE_BLOCK_KARAOKE; ?> ; ++i ) {
		if(document.getElementById('nombre_karas').value >= i && document.getElementById('sent_'+i).value==0) {
			
			console.log('currentform:'+i);
			current_form=i;
			submit_progress(i);
			return;
		}
	}
}


function submit_progress(id_form) {
	var xhr = new XMLHttpRequest();
	if(xhr) {
		xhr.open('POST', '?');

		xhr.upload.addEventListener("progress", uploadProgress, false);
		xhr.addEventListener("load", uploadComplete, false);
		xhr.addEventListener("error", uploadFailed, false);
		xhr.addEventListener("abort", uploadCanceled, false);

		var formData = new FormData(document.getElementById('form_'+id_form));
		
		formData.append("pseudo", document.getElementById('pseudo').value);
		formData.append("id", id_form);
	
		xhr.send(formData);
		
		document.getElementById('progression').style.display='';
	} else {
		alert('Navigateur récent avec ajax requis');
	}
}

function uploadProgress(evt) {
	if (evt.lengthComputable) {
		var percentCompleteprecise = Math.round(evt.loaded / evt.total * 10000)/100;
		document.getElementById('avancement_'+current_form).value=percentCompleteprecise;
		document.getElementById('pourcentage_'+current_form).innerHTML=' '+percentCompleteprecise+'% ('+Getunits(evt.loaded)+' / '+Getunits(evt.total)+')';
	}
}

function uploadComplete(evt) {
	if(evt.target.status!=200) return uploadFailed(evt);
	reponse=evt.target.responseText.trim();
	disable_kara_uploaded(current_form);
	document.getElementById('sent_'+current_form).value=1;
	document.getElementById('pourcentage_'+current_form).innerHTML=' Transfert fini.';
	if(reponse) {
		console.log(reponse);
		alert(reponse);
	}
	send_button();
}

function uploadFailed(evt) {
	document.getElementById('sent_'+current_form).value=0;
	alert('Il y a eu une erreur pendant l\'envoi.');
	document.getElementById('avancement_'+current_form).value=0;
	document.getElementById('pourcentage_'+current_form).innerHTML=' Erreur de transfert: '+evt.target.statusText + ' ('+evt.target.status+'). Cliquez sur Envoyer pour re-essayer.';
}

function uploadCanceled(evt) {
	document.getElementById('sent_'+current_form).value=0;
	alert('L\'envoi a été annulé.');
	document.getElementById('avancement_'+current_form).value=0;
	document.getElementById('pourcentage_'+current_form).innerHTML=' Transfert annulé. Cliquez sur Envoyer pour re-essayer.';
}
        </script>
    
</html> 
